import { Controller, Get, UseGuards, Req, BadGatewayException } from '@nestjs/common';
import { PermissionService } from './permission.service';
import { AuthGuard } from '@nestjs/passport';

@Controller('permissions')
export class PermissionController {
    constructor(private readonly permissionService: PermissionService) { }

    @Get('get-permissions')
    @UseGuards(AuthGuard('bearer'))
    async updateNotificationState(@Req() req) {
        const response: any = await this.permissionService.getPermissions(req.user.id);
        if (response)
            return response;
        throw new BadGatewayException(response);
    }
}

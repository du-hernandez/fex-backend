import { Module } from '@nestjs/common';
import { PermissionController } from './permission.controller';
import { PermissionService } from './permission.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { permission } from '../entities/permission';

@Module({
  imports: [
    TypeOrmModule.forFeature([permission])
  ],
  controllers: [PermissionController],
  providers: [PermissionService]
})
export class PermissionModule {}

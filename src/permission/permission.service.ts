import { Injectable } from '@nestjs/common';
import { permission } from '../entities/permission';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class PermissionService {
    constructor(
        @InjectRepository(permission) private readonly permissionRepository: Repository<permission>,
    ) { }

    async getPermissions(userId: number) {
        try {
            const permissions = await this.permissionRepository.createQueryBuilder("permission")
                .leftJoin("permission.user", "user")
                .where("user.id = :id", { id: userId })
                .getMany();
            return permissions;
        } catch (error) {
            return { error: 'TRANSACTION_ERROR', detail: error }
        }
    }
}

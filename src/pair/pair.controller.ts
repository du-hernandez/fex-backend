import { Controller, Post, Put, UseGuards, Body, BadGatewayException, Param, Get } from '@nestjs/common';
import { PairService } from './pair.service';
import { CommonPairDto } from './dto/commonPair.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('pair')
export class PairController {
    constructor(private readonly pairService: PairService) { }

    @Post()
    @UseGuards(AuthGuard('bearer'))
    async createPair(@Body() body: CommonPairDto) {
        const response: any = await this.pairService.createPair(body);
        if (response.success)
            return response;
        throw new BadGatewayException(response)
    }

    @Get(':id')
    @UseGuards(AuthGuard('bearer'))
    async getPairId(@Param('id') id: number){
        return this.pairService.getPairId(id);
    }

    @Get()
    @UseGuards(AuthGuard('bearer'))
    async getPairAll(){
        return this.pairService.getPairAll();
    }

    @Put(':id')
    @UseGuards(AuthGuard('bearer'))
    async updatePair(@Param('id') id: number, @Body() body: CommonPairDto) {
        const response: any = await this.pairService.updatePair(id, body);
        if (response.success)
            return response;
        throw new BadGatewayException(response)
    }

    @Put('change-state/:id')
    @UseGuards(AuthGuard('bearer'))
    async updatePairState(@Param('id') id: number) {
        const response: any = await this.pairService.updatePairState(id);
        if (response.success)
            return response;
        throw new BadGatewayException(response)
    }
}

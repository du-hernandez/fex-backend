import { Injectable } from '@nestjs/common';
import { pair } from '../entities/pair';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CommonPairDto } from './dto/commonPair.dto';

@Injectable()
export class PairService {

    constructor(
        @InjectRepository(pair) private readonly pairRepository: Repository<pair>
    ) { }

    async createPair(body: CommonPairDto) {
        try {
            const res = await this.pairRepository.save({ ...body })
            return (res.id) ? { success: 'OK', detail: res.id } : { error: 'TRANSACTION_ERROR' }
        } catch (error) {
            return { error: 'TRANSACTION_ERROR', detail: error }
        }
    }

    async getPairAll() {
        return await this.pairRepository.find()
    }

    async getPairId(pairId: number) {
        return await this.pairRepository.findOne(pairId)
    }

    async updatePair(pairId: number, body: CommonPairDto) {
        try {
            const res = await this.pairRepository.update(pairId, body);
            return res.raw.changedRows == 0 ? { error: 'NO_EXISTS' } : { success: 'OK' };
        } catch (error) {
            return { error: 'TRANSACTION_ERROR', detail: error }
        }
    }

    async updatePairState(pairId: number) {
        try {
            const current = await this.pairRepository.findOne(pairId, { select: ["state"] })

            if (!current) return { error: 'TRANSACTION_ERROR', detail: 'Invalided pair' };

            const res = await this.pairRepository.update(
                pairId,
                { state: (current.state == 'active') ? 'inactive' : 'active' }
            );
            return res.raw.changedRows == 0 ? { error: 'NO_UPDATE' } : { success: 'OK' };
        } catch (error) {
            return { error: 'TRANSACTION_ERROR', detail: error }
        }
    }
}

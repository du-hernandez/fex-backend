import { Module } from '@nestjs/common';
import { PairController } from './pair.controller';
import { PairService } from './pair.service';
import { pair } from '../entities/pair'
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forFeature([pair])
  ],
  controllers: [PairController],
  providers: [PairService]
})
export class PairModule {}

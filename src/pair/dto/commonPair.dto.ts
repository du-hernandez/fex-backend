import { Length } from 'class-validator';

export class CommonPairDto {

    @Length(7, 9)
    name: string;
}
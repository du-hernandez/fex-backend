const admin = require("firebase-admin");
const serviceAccount = require("../../../firebase-adminsdk.json");
import { InstanceConfigService } from '../config/config.service'

export const Templates = {
    CLOID_MESSAGING_DATA: {
        databaseURL: "https://fex-admin.firebaseio.com"
    }
};

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: Templates.CLOID_MESSAGING_DATA.databaseURL
});

export class CloudMessagingService {
    
    async sendNotification(data: any) {
        var message = {
            topic: InstanceConfigService.firebase.topic,
            notification: {
                title: data.tittle,
                body: data.body,
            },
            android: {
                priority: "normal",
                notification: {
                    sound: 'default',
                    color: '#f45342',
                },
            },
            apns: {
                headers: {
                    "apns-priority": "5"
                },
                payload: { aps: { sound: "default" } }
            },
            webpush: { headers: { Urgency: "high" } }
        };
        await admin.messaging().send(message)
            .then((response) => {
                // Response is a message ID string.
                console.log('Successfully sent message:', response);
            })
            .catch((error) => {
                console.log('Error sending message:', error);
            });
    }
}
import { InstanceConfigService } from '../config/config.service';

const sgMail = require('@sendgrid/mail');

export const Templates = {
    SENDGRID_DATA: {
        id: 'd-e9b7738d9e3447f4a9bb7ff4ee79156b',
        subject: 'New Feedback'
    }
};

export class SendgridService {

    constructor() {
        sgMail.setApiKey(InstanceConfigService.sendgrid.apiKey);
    }

    sendEmail(to: string, template: any, substitutions: any) {
        return new Promise((resolve, reject) => {
            const msg = {
                "from": { "email": "Fex<noreply@nativoadn.com>" },
                "personalizations": [{
                        "to": [{ "email": to }],
                        "dynamic_template_data": substitutions
                    }],
                "subject": template.subject || substitutions.subject,
                "template_id": template.id
            };
            sgMail.send(msg)
                .then(data => {
                    if (data[0] && data[0].complete)
                        resolve({ success: 'OK', ...data })
                    else
                        resolve({ success: 'ERROR', ...data })
                })
                .catch(err => resolve({ error: 'ERROR', ...err }));
        })
    }
}
import { Module, Global, Inject } from "@nestjs/common";
import { ConfigService } from './config/config.service'
import { SendgridService } from './utils/sendgrid.service'

@Global()
@Module({
  providers: [
    ConfigService,
    SendgridService
  ],
  exports: [
    ConfigService,
    SendgridService
  ]
})
export class CommonModule {}

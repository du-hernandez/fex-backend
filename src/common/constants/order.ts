export const ORDER = {
    sell_stop: 4,
    buy_stop: 5
}

export enum Order {
    sellStop = "sell_stop",
    buyStop = "buy_stop"
}

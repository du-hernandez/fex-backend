import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { video } from '../entities/video';
import { Repository } from 'typeorm';
import { CreateVideoDto } from './dto/createVideo.dto';

@Injectable()
export class VideoService {
    constructor(
        @InjectRepository(video) private readonly videoRepository: Repository<video>
    ) { }

    async createVideo(body: CreateVideoDto) {
        try {
            const res = await this.videoRepository.save({ ...body })
            return (res.id) ? { success: 'OK', detail: res.id } : { error: 'TRANSACTION_ERROR' }
        } catch (error) {
            return { error: 'TRANSACTION_ERROR', detail: error }
        }
    }

    async getVideoAll() {
        return await this.videoRepository.find()
    }

    async getVideoId(videoId: number) {
        return await this.videoRepository.findOne(videoId)
    }
}

import { IsEmail, Length, IsEnum } from 'class-validator';

enum Type {
    tutorial,
    academia
}

export class CreateVideoDto {
    
    @Length(3, 50)
    title: string;
    
    @Length(3, 300)
    description: string;

    @Length(3, 200)
    url: string;

    @IsEnum(Type)
    type: string;
}
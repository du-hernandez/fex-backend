import { Controller, BadGatewayException, Post, UseGuards, Get, Param, Body } from '@nestjs/common';
import { VideoService } from './video.service';
import { CreateVideoDto } from './dto/createVideo.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('video')
export class VideoController {

    constructor( private readonly videoService: VideoService){}

    @Post()
    @UseGuards(AuthGuard('bearer'))
    async createVideo(@Body() body: CreateVideoDto){
        const response: any = await this.videoService.createVideo(body);
        if(response.success)
            return response;
        throw new BadGatewayException(response)
    }

    @Get(':id')
    @UseGuards(AuthGuard('bearer'))
    async getViedoId(@Param('id') id: number){
        return this.videoService.getVideoId(id);
    }

    @Get()
    @UseGuards(AuthGuard('bearer'))
    async getViedoAll(){
        return this.videoService.getVideoAll();
    }
}

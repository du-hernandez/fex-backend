import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { feedback } from '../entities/feedback';
import { CreateFeedbackDto } from './dto/createFeedback.dto';

@Injectable()
export class FeedbackService {

    constructor(
        @InjectRepository(feedback) private readonly feedbackRepository: Repository<feedback>
    ) { }

    async createFeedback(userId: number, body: CreateFeedbackDto) {
        try {
            const res = await this.feedbackRepository.save({
                ...body,
                "user": {id: userId}
            })

            return (res.id) ? { success: 'OK', detail: res.id } : { error: 'TRANSACTION_ERROR' }
        } catch (error) {
            return { error: 'TRANSACTION_ERROR', detail: error }
        }
    }

    async getFeedbackAll() {
        return await this.feedbackRepository.find({
            relations:["user"]
        })
    }
}

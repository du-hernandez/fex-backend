import { IsEmail, Length } from 'class-validator';

export class CreateFeedbackDto {

    @Length(7, 15)
    mobile: string;

    @Length(10, 1000)
    message: string;
}
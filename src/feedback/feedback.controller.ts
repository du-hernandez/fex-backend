import { Controller, Body, Post, UseGuards, Req, BadGatewayException, Inject, Get } from '@nestjs/common';
import { FeedbackService } from './feedback.service';
import { CreateFeedbackDto } from './dto/createFeedback.dto';
import { AuthGuard } from '@nestjs/passport';
import { Templates } from '../common/utils/sendgrid.service';

@Controller('feedback')
export class FeedbackController {

  constructor(
    private readonly feedbackService: FeedbackService,
    @Inject('SendgridService') private readonly sendgridService
  ) {}

  @Get()
  @UseGuards(AuthGuard('bearer'))
  async getFeedbackAll(){
      return this.feedbackService.getFeedbackAll();
  }

  @Post()
  @UseGuards(AuthGuard('bearer'))
  async createFeedback(@Req() req, @Body() body: CreateFeedbackDto) {

    await this.sendgridService.sendEmail('info@tradingfex.com', Templates.SENDGRID_DATA, {...body, ...req.user})
    await this.sendgridService.sendEmail('yei.gomez@udla.edu.co', Templates.SENDGRID_DATA, {...body, ...req.user})

    const response: any = await this.feedbackService.createFeedback(req.user.id, body);
    if (response.success)
      return response;
    
    throw new BadGatewayException(response)
  }
}

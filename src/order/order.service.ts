import { Injectable } from '@nestjs/common';
import { order } from '../entities/order';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CommonOrderDto } from './dto/commonOrder.dto';

@Injectable()
export class OrderService{
    
    constructor(
        @InjectRepository(order) private readonly orderRepository: Repository<order>
    ) {}

    async createOrder(body: CommonOrderDto) {
        try {
            const res = await this.orderRepository.save({ ...body })
            return (res.id) ? { success: 'OK', detail: res.id } : { error: 'TRANSACTION_ERROR' }
        } catch (error) {
            return { error: 'TRANSACTION_ERROR', detail: error }
        }
    }

    async getOrderAll() {
        return await this.orderRepository.find()
    }

    async getOrderId(orderId: number) {
        return await this.orderRepository.findOne(orderId)
    }

    async updateOrder(orderId: number, body: CommonOrderDto) {
        try {
            const res = await this.orderRepository.update(orderId, body);
            return res.raw.changedRows == 0 ? { error: 'NO_EXISTS' } : { success: 'OK' };
        } catch (error) {
            return { error: 'TRANSACTION_ERROR', detail: error }
        }
    }

    async updateOrderState(orderId: number) {
        try {
            const current = await this.orderRepository.findOne(orderId, { select: ["state"] })

            if (!current) return { error: 'TRANSACTION_ERROR', detail: 'Invalided order' };

            const res = await this.orderRepository.update(
                orderId,
                { state: (current.state == 'active') ? 'inactive' : 'active' }
            );
            return res.raw.changedRows == 0 ? { error: 'NO_UPDATE' } : { success: 'OK' };
        } catch (error) {
            return { error: 'TRANSACTION_ERROR', detail: error }
        }
    }
}

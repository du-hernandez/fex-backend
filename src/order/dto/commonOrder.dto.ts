import { Length } from 'class-validator';

export class CommonOrderDto {

    @Length(2, 30)
    name: string;
}
import { Module } from '@nestjs/common';
import { OrderController } from './order.controller';
import { OrderService } from './order.service';
import { order } from '../entities/order'
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forFeature([order])
  ],
  controllers: [OrderController],
  providers: [OrderService]
})
export class OrderModule {}

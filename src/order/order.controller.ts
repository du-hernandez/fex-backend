import { Controller, Post, UseGuards, Body, BadGatewayException, Param, Get, Put } from '@nestjs/common';
import { OrderService } from './order.service';
import { AuthGuard } from '@nestjs/passport';
import { CommonOrderDto } from './dto/commonOrder.dto';

@Controller('order')
export class OrderController {
    constructor (private readonly orderService: OrderService){}

    @Post()
    @UseGuards(AuthGuard('bearer'))
    async createOrder(@Body() body: CommonOrderDto) {
        const response: any = await this.orderService.createOrder(body);
        if (response.success)
            return response;
        throw new BadGatewayException(response)
    }

    @Get(':id')
    @UseGuards(AuthGuard('bearer'))
    async getOrderId(@Param('id') id: number){
        return this.orderService.getOrderId(id);
    }

    @Get()
    @UseGuards(AuthGuard('bearer'))
    async getOrderAll(){
        return this.orderService.getOrderAll();
    }

    @Put(':id')
    @UseGuards(AuthGuard('bearer'))
    async updateOrder(@Param('id') id: number, @Body() body: CommonOrderDto) {
        const response: any = await this.orderService.updateOrder(id, body);
        if (response.success)
            return response;
        throw new BadGatewayException(response)
    }

    @Put('change-state/:id')
    @UseGuards(AuthGuard('bearer'))
    async updateOrderState(@Param('id') id: number) {
        const response: any = await this.orderService.updateOrderState(id);
        if (response.success)
            return response;
        throw new BadGatewayException(response)
    }
}

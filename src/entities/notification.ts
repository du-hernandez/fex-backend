import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, CreateDateColumn, OneToOne, OneToMany } from "typeorm";
import { signal } from "./signal";

@Entity("notification", { schema: "public" })
export class notification {

    @PrimaryGeneratedColumn({
        type: "bigint",
    })
    id: number;

    @Column("character varying", {
        length: 100
    })
    tittle: string;

    @Column("character varying", {
        length: 400
    })
    body: string;

    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    @ManyToOne(type => signal, signal => signal.notifications, { onUpdate: 'CASCADE', onDelete: 'CASCADE' })
    @JoinColumn({ name: "fk_signal" })
    signal: signal;
}

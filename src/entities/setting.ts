import { Entity, PrimaryGeneratedColumn, OneToMany, Column, JoinColumn, OneToOne } from "typeorm";
import { user } from "./user";

@Entity("setting", { schema: "public" })
export class setting {

    @PrimaryGeneratedColumn({
        type: "smallint"
    })
    id: number;
    
    @Column("character varying", {
        length: 10,
        default: "active"
    })
    notifications: string;
    
    @OneToOne(type=> user, user => user.setting, { onUpdate:'CASCADE', onDelete: 'CASCADE'})
    @JoinColumn({name: 'fk_user'})
    user: user;
}

import { Entity, PrimaryGeneratedColumn, ManyToMany, Column } from "typeorm";
import { user } from "./user";

@Entity("permission", { schema: "public" })
export class permission {

    @PrimaryGeneratedColumn({
        type: "smallint",
    })
    id: number;

    @Column("character varying", {
        length: 50,
        unique: true
    })
    name: string;

    @Column("character varying", {
        length: 20,
    })
    key: string;

    @ManyToMany(type => user, user => user.permission)
    user: user[];
}

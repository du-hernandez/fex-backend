import { Column, Entity, OneToMany, OneToOne, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToMany, JoinTable } from "typeorm";
import { trader } from './trader';
import { feedback } from "./feedback";
import { setting } from "./setting";
import { permission } from './permission';

@Entity("user", { schema: "public" })
export class user {

    @PrimaryGeneratedColumn({
        type: "bigint",
    })
    id: number;

    @Column("character varying", {
        length: 100
    })
    name: string;

    @Column("character varying", {
        length: 50
    })
    firstname: string;

    @Column("character varying", {
        length: 50
    })
    lastname: string;

    @Column("character varying", {
        length: 20,
        nullable: true
    })
    phone: string;

    @Column("character varying", {
        length: 100,
        unique: true
    })
    email: string;

    @Column("text", {
        select: false,
        nullable: true
    })
    password: string;

    @Column("text", {
        nullable: true
    })
    token: string;

    @Column("character varying", {
        length: 10,
        default: "active"
    })
    state: string;

    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;

    @OneToMany(type => feedback, feedback => feedback.user, { onDelete: 'RESTRICT', onUpdate: 'RESTRICT' })
    feedbacks: feedback[];

    @OneToOne(type => trader, trader => trader.user)
    trader: trader;

    @OneToOne(type => setting, setting => setting.user)
    setting: setting;

    @ManyToMany(type => permission, permission => permission.user)
    @JoinTable()
    permission: permission[];
}

import { Entity, PrimaryGeneratedColumn, OneToMany, Column } from "typeorm";

@Entity("video", { schema: "public" })
export class video {

    @PrimaryGeneratedColumn({
        type: "int"
    })
    id: number;

    @Column("character varying",{
        length:50
    })
    title: string;

    @Column("character varying",{
        length:300
    })
    description: string;

    @Column("character varying",{
        length:200,
        unique: true
    })
    url: string;

    @Column("character varying",{
        length:200
    })
    type: string;
}

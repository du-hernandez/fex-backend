import { Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn, Column, UpdateDateColumn, CreateDateColumn, ManyToMany, OneToMany, OneToOne } from "typeorm";
import { order } from "./order";
import { pair } from "./pair";
import { trader } from "./trader";
import { notification } from "./notification";

@Entity("signal", { schema: "public" })
export class signal {

    @PrimaryGeneratedColumn({
        type: "bigint"
    })
    id: number;

    @Column("character varying")
    transaction: string;

    @Column("decimal", {
        nullable: true,
        name: "entry_price"
    })
    entryPrice: number;

    @Column("decimal", {
        name: "take_profit",
        nullable: true
    })
    takeProfit: number;

    @Column("decimal", {
        nullable: true
    })
    pips: number;

    @Column("decimal", {
        name: "stop_loss",
        nullable: true
    })
    stopLoss: number;

    @Column("character varying", {
        name: "risk_management",
        nullable: true,
    })
    riskManagement: string;

    @Column("character varying", {
        length: 10,
        default: "active"
    })
    state: string;

    @Column("character varying", {
        length: 200,
        nullable: true
    })
    observation: string;

    @CreateDateColumn({ type: "timestamp without time zone" })
    createdAt: Date;

    @UpdateDateColumn({ type: "timestamp without time zone" })
    updatedAt: Date;

    @ManyToOne(type => order, order => order.signals, { nullable: false, onDelete: 'SET NULL', onUpdate: 'CASCADE' })
    @JoinColumn({ name: "fk_order" })
    order: order;

    @ManyToOne(type => pair, pair => pair.signals, { nullable: true, onDelete: 'SET NULL', onUpdate: 'CASCADE' })
    @JoinColumn({ name: 'fk_pair' })
    pair: pair;

    @ManyToOne(type => trader, trader => trader.signals, { nullable: true, onUpdate: 'CASCADE', onDelete: 'CASCADE' })
    @JoinColumn({ name: 'fk_trader' })
    trader: trader;

    @OneToOne(type => signal, signal => signal.signal, { nullable: true, onUpdate: 'CASCADE', onDelete: 'CASCADE' })
    @JoinColumn({ name: 'fk_signal' })
    signal: signal;

    @OneToMany(type => notification, notification => notification.signal, { onUpdate: "CASCADE", onDelete: "CASCADE" })
    notifications: notification[];
}

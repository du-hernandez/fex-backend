import { Entity, PrimaryGeneratedColumn, OneToMany, Column } from "typeorm";
import { signal } from "./signal";

@Entity("order", { schema: "public" })
export class order {

    @PrimaryGeneratedColumn({
        type: "smallint"
    })
    id: number;

    @Column("character varying",{
        length:50,
        unique:true
    })
    name: string;

    @Column("character varying", {
        length: 10,
        default: "active"
    })
    state: string;

    @OneToMany(type=> signal, signal=>signal.order)
    signals: signal[];
}

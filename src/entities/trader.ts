import { BaseEntity, Column, Entity, Index, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, RelationId, CreateDateColumn } from "typeorm";
import { user } from './user';
import { signal } from "./signal";

@Entity("trader", { schema: "public" })
export class trader {

    @PrimaryGeneratedColumn({
        type: "smallint"
    })
    id: number;

    @Column("character varying", {
        length: 10,
        default: "active"
    })
    state: string;

    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    @OneToOne(type=> user, user => user.trader, { onUpdate:'CASCADE', onDelete: 'CASCADE'})
    @JoinColumn({name: 'fk_user'})
    user: user;

    @OneToMany(type=> signal, signal => signal.trader)
    signals: signal[];
}

import { Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn, Column, CreateDateColumn } from "typeorm";
import { user } from "./user";

@Entity("feedback", { schema: "public" })
export class feedback {

    @PrimaryGeneratedColumn({
        type: "int"
    })
    id: number;

    @Column("character varying", {
        length: 15
    })
    mobile: string;

    @Column("text")
    message: string;

    @Column("character varying",{
        length:10,
        default: "N/A"
    })
    type: string;

    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    @ManyToOne(type => user, user => user.feedbacks, { nullable:false, onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    @JoinColumn({ name: 'fk_user' })
    user: user;
}

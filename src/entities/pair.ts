import { Entity, PrimaryGeneratedColumn, OneToMany, Column } from "typeorm";
import { signal } from "./signal";

@Entity("pair", { schema: "public" })
export class pair {

    @PrimaryGeneratedColumn({
        type: "smallint"
    })
    id: number;

    @Column("character varying",{
        length:20,
        unique:true
    })
    name: string;
    
    @Column("character varying", {
        length: 10,
        default: "active"
    })
    state: string;

    @OneToMany(type=> signal, signal=>signal.pair)
    signals: signal[];
}

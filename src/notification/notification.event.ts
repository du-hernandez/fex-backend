import { EventEmitter } from 'events';
import { StrictEventEmitter } from 'nest-emitter';

interface NotificationEvents {
    notifications: (notifications: any) => void;
    newRequest: (req: Express.Request) => void;
}

export type NotificationEventsEmitter = StrictEventEmitter<EventEmitter, NotificationEvents>;
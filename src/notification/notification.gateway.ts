import { OnModuleInit } from '@nestjs/common';
import { WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { InjectEventEmitter } from 'nest-emitter';
import { NotificationEventsEmitter } from './notification.event';

@WebSocketGateway()
export class NotificationGateway implements OnModuleInit {

    @WebSocketServer() server;

    constructor(
        @InjectEventEmitter() private readonly emitter: NotificationEventsEmitter
    ) { }

    onModuleInit() {
        this.emitter.on('notifications', async msg => await this.onNotification(msg));
    }

    private async onNotification(notifications) {
        this.server.emit(`notifications`, notifications);
    }
}
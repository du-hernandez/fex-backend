import { Length, IsNumber, IsJSON, IsOptional } from 'class-validator';

export class CreateNotificationDto {

    @Length(2, 50)
    tittle: string;
    
    @Length(2, 300)
    body: string;
    
    @IsNumber()
    @IsOptional()
    signal: number;
}
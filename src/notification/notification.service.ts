import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { notification } from '../entities/notification';
import { CreateNotificationDto } from './dto/createNotification.dto';

@Injectable()
export class NotificationService {

    constructor(
        @InjectRepository(notification) private readonly notificationRepository: Repository<notification>
    ) { }

    async createNotification(body: CreateNotificationDto) {
        try {
            const params: any = {};
            if (body.signal)
                params.signal = { id: body.signal };

            const res = await this.notificationRepository.save({ ...body, ...params });

            const newNotification = await this.getNotificationAll({ page: 1 }, res.id);

            return (res.id) ? { success: 'OK', detail: newNotification } : { error: 'TRANSACTION_ERROR' };
        } catch (error) {
            return { error: 'TRANSACTION_ERROR', detail: error }
        }
    }


    async getNotificationAll(body, id?) {
        const query = await this.notificationRepository.createQueryBuilder().select()

        if (id) query.where("notification.id = :notificationId", { notificationId: id })

        query.offset(((body.page * 5) - (4)) - 1)
            .limit(5)
            .orderBy("notification.createdAt", "DESC")

        return { data: await query.execute(), pages: Math.ceil(await query.getCount() / 5) }
    }

    async getNotificationId(notificationId: number) {
        return await this.notificationRepository.findOne(notificationId)
    }
}

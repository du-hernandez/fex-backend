import { Module } from '@nestjs/common';
import { NotificationController } from './notification.controller';
import { NotificationService } from './notification.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { notification } from '../entities/notification';
import { signal } from '../entities/signal';
import { NotificationGateway } from './notification.gateway';
import { CloudMessagingService } from '../common/utils/cloudMessaging.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([signal, notification])
  ],
  controllers: [NotificationController],
  providers: [
    NotificationService,
    NotificationGateway,
    CloudMessagingService,
  ]
})
export class NotificationModule {}

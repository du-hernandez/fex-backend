import { Controller, UseGuards, Get, Param, BadGatewayException, Body, Post, Query } from '@nestjs/common';
import { NotificationService } from './notification.service';
import { CreateNotificationDto } from './dto/createNotification.dto';
import { AuthGuard } from '@nestjs/passport';
import { NotificationEventsEmitter } from './notification.event';
import { CloudMessagingService } from '../common/utils/cloudMessaging.service';
import { InjectEventEmitter } from 'nest-emitter';

@Controller('notification')
export class NotificationController {
    constructor(
        private readonly notificationService: NotificationService,
        private readonly cloudMessagingService: CloudMessagingService,
        @InjectEventEmitter() private readonly notificationEmitter: NotificationEventsEmitter
    ) { }

    @Post()
    @UseGuards(AuthGuard('bearer'))
    async createNotification(@Body() body: CreateNotificationDto) {
        const response: any = await this.notificationService.createNotification(body);
        if (response.success) {
            const newNoti = response.detail.data[0];
            const data = {
                tittle: newNoti.notification_tittle,
                body: newNoti.notification_body,
                signal: newNoti.notification_fk_signal,
            }

            this.notificationEmitter.emit("notifications", response.detail)
            await this.cloudMessagingService.sendNotification(data);
            return response;
        }
        throw new BadGatewayException(response)
    }

    @Get(':id')
    @UseGuards(AuthGuard('bearer'))
    async getNotificationId(@Param('id') id: number) {
        return this.notificationService.getNotificationId(id);
    }

    @Get()
    @UseGuards(AuthGuard('bearer'))
    async getNotificationAll(@Query() query: { page: string }) {
        return this.notificationService.getNotificationAll(query);
    }
}

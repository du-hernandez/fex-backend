import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { OrderModule } from './order/order.module';
import { PairModule } from './pair/pair.module';
import { SignalModule } from './signal/signal.module';
import { FeedbackModule } from './feedback/feedback.module';
import { NotificationModule } from './notification/notification.module';
import { VideoModule } from './video/video.module';
import { CommonModule } from './common/common.module';
import { InstanceConfigService } from './common/config/config.service';
import { TraderModule } from './trader/trader.module';
import { NestEmitterModule } from 'nest-emitter';
import { EventEmitter } from 'events';
import { SettingModule } from './setting/setting.module';
import { PermissionModule } from './permission/permission.module';


@Module({
  imports: [
    TypeOrmModule.forRoot(InstanceConfigService.orm_config),
    CommonModule,
    UserModule,
    AuthModule,
    OrderModule,
    PairModule,
    SignalModule,
    FeedbackModule,
    NotificationModule,
    VideoModule,
    TraderModule,
    SettingModule,
    PermissionModule,
    NestEmitterModule.forRoot(new EventEmitter())
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

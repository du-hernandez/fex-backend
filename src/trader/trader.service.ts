import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { user } from '../entities/user';
import { Repository, getManager } from 'typeorm';
import { trader } from '../entities/trader';
import { CreateTraderDto } from './dto/createTrader.dto';

@Injectable()
export class TraderService {

    constructor(
        @InjectRepository(user) private readonly userRepository: Repository<user>,
        @InjectRepository(trader) private readonly traderRepository: Repository<trader>
    ) { }

    async createTrader(body: CreateTraderDto) {
        try {
            let res;
            await getManager().transaction(async entityManager => {
                const { sha256 } = require('crypto-hash');
                const user = await entityManager.save(
                    await this.userRepository.create({ ...body, password: await sha256(body.password) })
                );
                await entityManager.save(
                    res = await this.traderRepository.create({ user: { id: user.id } })
                );
            });
            return res.id == 0 ? { error: 'TRANSACTION_ERROR' } : { success: 'OK', detail: res.id };
        } catch (error) {
            return { error: 'TRANSACTION_ERROR', detail: error };
        }
    }

    async getTraderAll() {
        return await this.traderRepository.find({
            join: {
                alias: "trader",
                innerJoinAndSelect: {
                    user: "trader.user"
                }
            }
        })
    }
}

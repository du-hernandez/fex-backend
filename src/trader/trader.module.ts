import { Module } from '@nestjs/common';
import { TraderController } from './trader.controller';
import { TraderService } from './trader.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { trader } from '../entities/trader';
import { user } from '../entities/user';

@Module({
  imports:[
    TypeOrmModule.forFeature([trader, user])
  ],
  controllers: [TraderController],
  providers: [TraderService]
})
export class TraderModule {}

import { Controller, Post, Body, BadGatewayException, UseGuards, Get } from '@nestjs/common';
import { TraderService } from './trader.service';
import { CreateTraderDto } from './dto/createTrader.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('trader')
export class TraderController {
    constructor(private readonly traderService: TraderService) { }

    @Post()
    async createTrader(@Body() body: CreateTraderDto) {
        const response: any = await this.traderService.createTrader(body);
        if (response.success)
            return response;
        throw new BadGatewayException(response)
    }

    @Get()
    @UseGuards(AuthGuard('bearer'))
    async getTraderAll(){
        return this.traderService.getTraderAll();
    }
}

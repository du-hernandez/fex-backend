import { Injectable, HttpService } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { user } from '../entities/user';
import { Repository } from 'typeorm';
import { AuthDto } from './dto/auth.dto';
import { UserService } from '../user/user.service';
import { ok } from 'assert';

@Injectable()
export class AuthService {

    constructor(
        @InjectRepository(user)
        private readonly userRepository: Repository<user>,
        private readonly jwtService: JwtService,
        private readonly httpService: HttpService,
        private readonly userService: UserService
    ) { }

    async login(body: AuthDto) {
        console.log(body.email);
        
        const requestBody = {
            usuario: body.email,
            clave: body.password,
            apikey: "8qVBQGVk-4viA-4pdS-121YRrHKlCL1"
        }

        const resApi = await this.httpService.post(
            'https://back.tradingfex.com/millenium/api-backtradingfex/validarDatosAfiliado',
            require('querystring').stringify(requestBody),
            { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }
        ).toPromise()

        console.log(resApi.data);
        
        if (resApi.data.status == 'Error') return { error: "USER_NOT_EXIST", detail: "El usuario no existe." };
        if (resApi.data.datos.estado == 'Inactivo') return { error: "USER_INACTIVED", detail: "El usuario aún no ha sido activado." };

        const res = await this.userRepository
            .createQueryBuilder("user")
            .select(['user.id', 'user.name', 'user.email'])
            .where("user.email = :email and user.name = :name", { email: resApi.data.datos.correo.toLowerCase(), name: resApi.data.datos.usuario })
            .getOne();
        if (res) return res;

        const data = {
            name: resApi.data.datos.usuario,
            email: resApi.data.datos.correo,
            firstname: resApi.data.datos.nombre,
            lastname: resApi.data.datos.apellido,
            phone: resApi.data.datos.telefono
        }

        const response: any = await this.userService.createUser(data);
        if (response.error) return undefined;

        return { id: response.detail, name: resApi.data.datos.usuario, email: resApi.data.datos.correo }
    }

    async loginAdmin(body: AuthDto) {
        const { sha256 } = require('crypto-hash');
        return await this.userRepository
            .createQueryBuilder("user")
            .select(['user.id', 'user.name', 'user.email'])
            .where("user.email = :email and user.password = :password", { email: body.email.toLowerCase(), password: await sha256(body.password) })
            .getOne();
    }

    async validateUser(userToken: string): Promise<any> {
        let payload: any = this.jwtService.decode(userToken);
        if (payload) {
            let response = await this.userRepository.findOne({ select: ['id', 'email', 'name'], where: { ...payload, token: userToken } })
            if (response)
                return response;

            return false;
        }
        return false;
    }

    async updateToken(userId: number, userToken: string) {
        try {
            const res = await this.userRepository.update(userId, { token: userToken });
            return res.raw.changedRows == 0 ? { error: 'NO_UPDATE' } : { payload: userToken };
        } catch (error) {
            return { error: 'TRANSACTION_ERROR', detail: error }
        }
    }
}

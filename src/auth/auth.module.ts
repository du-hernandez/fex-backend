import { Module, HttpModule } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';
import { user } from '../entities/user';
import { HttpStrategy } from '../common/strategys/http.strategy';
import { UserService } from '../user/user.service';

@Module({
  imports:[
    TypeOrmModule.forFeature([user]), 
    JwtModule.register({ secret: 'abcd' }),
    HttpModule
  ],
  controllers: [AuthController],
  providers: [AuthService, HttpStrategy, UserService]
})
export class AuthModule {}

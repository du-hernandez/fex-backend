import { IsEmail, MaxLength, Length, IsNumber, IsString, IsDate, MinDate, IsNotEmpty, IsEmpty, IsEnum, IsOptional } from 'class-validator';

enum Transaction {
    compra = "COMPRA",
    venta = "VENTA"
}

export class CreateSignalDto {

    @IsNumber()
    pair: number;

    @IsNumber()
    order: number;

    @IsEnum(Transaction)
    transaction: string;

    @IsNumber()
    @IsOptional()
    entryPrice: number;

    @IsNumber()
    @IsOptional()
    stopLoss: number;

    @IsNumber()
    @IsOptional()
    takeProfit: number;

    @IsString()
    riskManagement: string;
}
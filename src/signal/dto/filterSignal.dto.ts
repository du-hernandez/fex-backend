import { IsNumber, IsDate, IsOptional } from 'class-validator';

export class FilterSignalDto {

    @IsOptional()
    trader: number;

    @IsOptional()
    pair: number;

    @IsDate()
    @IsOptional()
    dateInit: Date;

    @IsDate()
    @IsOptional()
    dateEnd: Date;

    @IsOptional()
    pipsMin: number;

    @IsOptional()
    pipsMax: number;

    page: number;

    offset: number;
}
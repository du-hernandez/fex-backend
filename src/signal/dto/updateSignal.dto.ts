import { IsNumber, IsEnum, MaxLength, IsString } from 'class-validator';

enum Transaction {
    compra = "COMPRA",
    venta = "VENTA"
}

export class UpdateSignalDto {

    @IsNumber()
    pair: number;

    @IsNumber()
    order: number;

    @IsEnum(Transaction)
    transaction: string;

    @IsNumber()
    entryPrice: number;

    @IsNumber()
    stopLoss: number;

    @IsNumber()
    takeProfit: number;

    @IsString()
    riskManagement: string;

    @MaxLength(20)
    observation: string;
}
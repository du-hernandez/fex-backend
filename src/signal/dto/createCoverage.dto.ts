import { IsNumber, IsEnum, IsString } from 'class-validator';

export enum Order {
    sellStop = "sell_stop",
    buyStop = "buy_stop"
}

export class CreateCoverageDto {

    @IsEnum(Order)
    order: string;

    @IsNumber()
    entryPrice: number;

    @IsString()
    riskManagement: string;

    @IsNumber()
    signal: number;
}
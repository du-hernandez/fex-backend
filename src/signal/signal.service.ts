import { Injectable, Req, HttpService } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getManager } from 'typeorm';
import { signal } from '../entities/signal';
import { FilterSignalDto } from './dto/filterSignal.dto';
import { CreateSignalDto } from './dto/createSignal.dto';
import { CreateCoverageDto } from './dto/createCoverage.dto';
import { UpdateSignalDto } from './dto/updateSignal.dto';
import { ORDER } from '../common/constants/order';
import { trader } from '../entities/trader';

@Injectable()
export class SignalService {

    constructor(
        @InjectRepository(signal) private readonly signalRepository: Repository<signal>,
        @InjectRepository(trader) private readonly traderRepository: Repository<trader>,
        private readonly httpService: HttpService
    ) { }

    async createSignal(userId: number, body: CreateSignalDto) {
        try {
            const trader = await this.traderRepository.createQueryBuilder()
                .select("trader.id")
                .where("fk_user = :id", { id: userId })
                .getOne()

            const res = await this.signalRepository.save({
                ...body,
                order: { id: body.order },
                pair: { id: body.pair },
                trader: { id: trader.id }
            })

            const newSignal = await this.getSignalAll({ page: 1 }, res.id)

            return (res.id) ? { success: 'OK', detail: newSignal } : { error: 'TRANSACTION_ERROR' };
        } catch (error) {
            return { error: 'TRANSACTION_ERROR', detail: error };
        }
    }

    async createCoverage(body: CreateCoverageDto) {
        try {
            const current = await this.signalRepository.findOne(body.signal, {
                select: ["transaction"],
                where: { signal: null }
            })

            if (!current) return { error: 'TRANSACTION_ERROR', detail: 'Invalided signal' };

            const res = await this.signalRepository.save({
                ...body,
                order: ORDER[body.order],
                transaction: (current.transaction == "COMPRA") ? "VENTA" : "COMPRA",
                signal: { id: body.signal }
            })
            return (res.id) ? { success: 'OK', detail: res.id } : { error: 'TRANSACTION_ERROR' };

        } catch (error) {
            return { error: 'TRANSACTION_ERROR', detail: error };
        }
    }

    async getSignalAll(body, id?) {
        const query = await this.signalRepository.createQueryBuilder()
            .select(["signal.id", "signal.transaction", "signal.take_profit", "signal.stop_loss", "signal.pips", "signal.state", "signal.observation"])
            .addSelect("signal.updatedAt - interval '5 hours'", "signal_updatedAt")
            .addSelect("signal.createdAt - interval '5 hours'", "signal_createdAt")
            .addSelect("signal.entry_price", "signal_entry_price")
            .addSelect("signal.risk_management", "signal_risk_management")
            .addSelect(["coverage.transaction", "coverage.createdAt"])
            .addSelect("coverage.entry_price", "coverage_entry_price")
            .addSelect("coverage.risk_management", "coverage_risk_management")
            .addSelect("orderSignal.name")
            .addSelect("orderCoverage.name")
            .addSelect("pair.name")

            .innerJoin("signal.pair", "pair")
            .innerJoin("signal.order", "orderSignal")
            .leftJoin("signal", "coverage", "coverage.fk_signal = signal.id")
            .leftJoin("coverage.order", "orderCoverage")

        if (id) query.where("signal.id = :signalId", { signalId: id })
        else query.where("signal.state = 'active'")

        query.offset(((body.page * 5) - (4)) - 1)
            .limit(5)
            .orderBy("signal.updatedAt", "DESC")

        return { data: await query.execute(), pages: Math.ceil(await query.getCount() / 5) }
    }


    async getSignalAdmin() {
        return await this.signalRepository.createQueryBuilder()
            .select(["signal.id", "signal.transaction", "signal.take_profit", "signal.stop_loss", "signal.pips", "signal.state", "signal.observation"])
            .addSelect("signal.updatedAt - interval '5 hours'", "signal_updatedAt")
            .addSelect("signal.createdAt - interval '5 hours'", "signal_createdAt")
            .addSelect("signal.entry_price", "signal_entry_price")
            .addSelect("signal.risk_management", "signal_risk_management")
            .addSelect(["coverage.transaction", "coverage.createdAt"])
            .addSelect("coverage.entry_price", "coverage_entry_price")
            .addSelect("coverage.risk_management", "coverage_risk_management")
            .addSelect("orderSignal.name")
            .addSelect("orderCoverage.name")
            .addSelect("pair.name")

            .innerJoin("signal.pair", "pair")
            .innerJoin("signal.order", "orderSignal")
            .leftJoin("signal", "coverage", "coverage.fk_signal = signal.id")
            .leftJoin("coverage.order", "orderCoverage")
            .orderBy("signal.state", "ASC")
            .addOrderBy("signal.updatedAt", "DESC")
            .execute()
    }

    async getSignalId(id) {
        return await this.signalRepository.createQueryBuilder()
            .select(["signal.id", "signal.transaction", "signal.take_profit", "signal.stop_loss", "signal.pips", "signal.state", "signal.observation"])
            .addSelect("signal.updatedAt - interval '5 hours'", "signal_updatedAt")
            .addSelect("signal.createdAt - interval '5 hours'", "signal_createdAt")
            .addSelect("signal.entry_price", "signal_entry_price")
            .addSelect("signal.risk_management", "signal_risk_management")
            .addSelect(["coverage.transaction", "coverage.createdAt"])
            .addSelect("coverage.entry_price", "coverage_entry_price")
            .addSelect("coverage.risk_management", "coverage_risk_management")
            .addSelect(["orderSignal.name", "orderSignal.id"])
            .addSelect("orderCoverage.name")
            .addSelect(["pair.name", "pair.id"])

            .innerJoin("signal.pair", "pair")
            .innerJoin("signal.order", "orderSignal")
            .leftJoin("signal", "coverage", "coverage.fk_signal = signal.id")
            .leftJoin("coverage.order", "orderCoverage")
            .where("signal.id = :signalId", { signalId: id })
            .getRawOne()
    }

    async getSignalFilter(body: FilterSignalDto) {

        const query = await this.signalRepository.createQueryBuilder()
            .select(["signal.id", "signal.pips", "signal.createdAt", "signal.updatedAt"])
            .addSelect("user.name")
            .addSelect("pair.name")
            .addSelect("trader.id")
            .innerJoin("signal.pair", "pair")
            .innerJoin("signal.trader", "trader")
            .innerJoin("trader.user", "user")
            .where("signal.state = 'inactive'")

        if (body.pair) query.andWhere("pair.id = :pairId", { pairId: body.pair })
        if (body.trader) query.andWhere("user.id = :userId", { userId: body.trader })
        if (body.dateInit) query.andWhere("signal.createdAt >= :dateInit", { dateInit: body.dateInit })
        if (body.dateEnd) query.andWhere("signal.createdAt <= :dateEnd", { dateEnd: body.dateEnd })
        if (body.pipsMin) query.andWhere("signal.pips >= :pipsMin", { pipsMin: body.pipsMin })
        if (body.pipsMax) query.andWhere("signal.pips <= :pipsMax", { pipsMax: body.pipsMax })

        query.offset(((body.page * body.offset) - (body.offset - 1)) - 1)
            .limit(body.offset)
            .orderBy("signal.updatedAt", "DESC")

        const [response, total] = await query.getManyAndCount();
        return { data: response, pages: Math.ceil(total / body.offset) }
    }

    async updateSignal(signalId: number, body: UpdateSignalDto) {
        try {
            const res = await this.signalRepository.update(
                signalId,
                {
                    ...body,
                    order: { id: body.order },
                    pair: { id: body.pair },
                }
            );
            return res.raw.changedRows == 0 ? { error: 'NO_EXISTS' } : { success: 'OK' };
        } catch (error) {
            return { error: 'TRANSACTION_ERROR', detail: error }
        }
    }

    async updateSignalObservation(signalId: number, observation: string) {
        try {
            const res = await this.signalRepository.update(
                signalId,
                { observation }
            );
            return res.raw.changedRows == 0 ? { error: 'NO_EXISTS' } : { success: 'OK' };
        } catch (error) {
            return { error: 'TRANSACTION_ERROR', detail: error }
        }
    }

    async updateSignalState(body: { signalId: number, pairName: string, entryPrice: number }) {
        try {
            const current = await this.signalRepository.findOne(body.signalId, { select: ["state", "pips"] })

            if (!current) return { error: 'TRANSACTION_ERROR', detail: 'Invalided signal' }

            if (body.entryPrice != 0) {

                const requestBody = {
                    filter: [{
                        left: "name",
                        operation: "match",
                        right: body.pairName.replace('/', '')
                    }],
                    columns: ["close"]
                }

                const resApi = await this.httpService.post(
                    'https://scanner.tradingview.com/forex/scan',
                    requestBody
                ).toPromise()

                var signalPips = Number(await this.calculePips(resApi.data.data[0].d[0], body.entryPrice))
            }

            const res = await this.signalRepository.update(
                { id: body.signalId },
                {
                    state: (current.state == 'active') ? 'inactive' : 'active',
                    pips: (current.state == 'active' && body.entryPrice != 0) ? signalPips : current.pips
                }
            );
            return res.raw.changedRows == 0 ? { error: 'NO_UPDATE' } : { success: 'OK' };
        } catch (error) {
            return { error: 'TRANSACTION_ERROR', detail: error }
        }
    }

    calculePips(currentPrice: number, entryPrice: number) {
        const currentPriceDec = currentPrice.toString().split(".")[1].length;
        const entryPriceDec = entryPrice.toString().split(".")[1].length;
        const decimal = currentPriceDec > entryPriceDec ? currentPriceDec : entryPriceDec;
        const difference = currentPrice - entryPrice;
        return (difference == 0) ? difference : Number((difference).toFixed(decimal).toString().split(".")[1]);
    }
}

import { EventEmitter } from 'events';
import { StrictEventEmitter } from 'nest-emitter';

interface SignalEvents {
    signals: (signals: any) => void;
    newRequest: (req: Express.Request) => void;
}

export type SignalEventEmitter = StrictEventEmitter<EventEmitter, SignalEvents>;
import { Controller, Get, UseGuards, Req, Post, Body, Query, BadGatewayException, Put, Param } from '@nestjs/common';
import { SignalService } from './signal.service';
import { FilterSignalDto } from './dto/filterSignal.dto';
import { AuthGuard } from '@nestjs/passport';
import { CreateSignalDto } from './dto/createSignal.dto';
import { CreateCoverageDto } from './dto/createCoverage.dto';
import { UpdateSignalDto } from './dto/updateSignal.dto';
import { InjectEventEmitter } from 'nest-emitter';
import { SignalEventEmitter } from './signal.event';
import { NotificationService } from '../notification/notification.service';
import { CloudMessagingService } from '../common/utils/cloudMessaging.service';
import { NotificationEventsEmitter } from '../notification/notification.event';

@Controller('signal')
export class SignalController {
  constructor(
    private readonly signalService: SignalService,
    private readonly notificationService: NotificationService,
    private readonly cloudMessagingService: CloudMessagingService,
    @InjectEventEmitter() private readonly signalEmitter: SignalEventEmitter,
    @InjectEventEmitter() private readonly notificationEmitter: NotificationEventsEmitter
  ) { }

  @Get('all')
  @UseGuards(AuthGuard('bearer'))
  async getSignalAll(@Query() query: { page: number }) {
    return await this.signalService.getSignalAll(query);
  }

  @Get('id/:id')
  @UseGuards(AuthGuard('bearer'))
  async getSignalId(@Param('id') id: number) {
    return await this.signalService.getSignalId(id);
  }

  @Get('admin')
  @UseGuards(AuthGuard('bearer'))
  async getSignalAdmin() {
    return await this.signalService.getSignalAdmin();
  }

  @Get('filter')
  @UseGuards(AuthGuard('bearer'))
  async getSignalFilter(@Query() query: FilterSignalDto) {
    return await this.signalService.getSignalFilter(query);
  }

  @Post()
  @UseGuards(AuthGuard('bearer'))
  async createSignal(@Req() req, @Body() body: CreateSignalDto) {
    const response: any = await this.signalService.createSignal(req.user.id, body)
    if (response.success) {
      const newSignal = response.detail.data[0];
      this.signalEmitter.emit("signals", response.detail)
      const data = {
        tittle: `Nueva señal creada`,
        body: `Par: ${newSignal.pair_name} \nTransacción:  ${newSignal.signal_transaction} \nOrden: ${newSignal.orderSignal_name}`,
        signal: newSignal.signal_id
      }
      const noti = await this.notificationService.createNotification(data)
      this.notificationEmitter.emit("notifications", noti.detail)
      await this.cloudMessagingService.sendNotification(data);
      return response;
    }
    throw new BadGatewayException(response)
  }

  @Post('coverage')
  @UseGuards(AuthGuard('bearer'))
  async createCoverage(@Body() body: CreateCoverageDto) {
    const response: any = await this.signalService.createCoverage(body);
    if (response.success)
      return response;
    throw new BadGatewayException(response)
  }

  @Put(':id')
  @UseGuards(AuthGuard('bearer'))
  async updateSignal(@Param('id') id: number, @Body() body: UpdateSignalDto) {
    const response: any = await this.signalService.updateSignal(id, body);
    if (response.success) {
      /* this.signalEmitter.emit("signals", response.detail) */
      const newSignal = await this.signalService.getSignalAll({ page: 1 }, id)
      const data = {
        tittle: 'Señal actualizada',
        body: `Par: ${newSignal.data[0].pair_name} \nTransacción:  ${newSignal.data[0].signal_transaction} \nOrden: ${newSignal.data[0].orderSignal_name}`,
        signal: id
      }
      const noti = await this.notificationService.createNotification(data)
      this.notificationEmitter.emit("notifications", noti.detail)
      await this.cloudMessagingService.sendNotification(data);
      return response;
    }
    throw new BadGatewayException(response)
  }

  @Put('observation/:id')
  @UseGuards(AuthGuard('bearer'))
  async updateSignalObservation(@Param('id') id: number, @Body() body: { observation: string }) {
    const response: any = await this.signalService.updateSignalObservation(id, body.observation);
    if (response.success) {
      const newSignal = await this.signalService.getSignalAll({ page: 1 }, id)
      const data = {
        tittle: `Actualización: ${body.observation}`,
        body: `Par: ${newSignal.data[0].pair_name} \nTransacción:  ${newSignal.data[0].signal_transaction} \nOrden: ${newSignal.data[0].orderSignal_name}`,
        signal: id
      }
      const noti = await this.notificationService.createNotification(data)
      this.notificationEmitter.emit("notifications", noti.detail)
      await this.cloudMessagingService.sendNotification(data);
      return response;
    }
    throw new BadGatewayException(response)
  }

  @Put('change/state')
  @UseGuards(AuthGuard('bearer'))
  async updateSignalState(@Body() body: { signalId: number, pairName: string, entryPrice: number }) {
    const response: any = await this.signalService.updateSignalState(body);
    if (response.success) return response;

    throw new BadGatewayException(response)
  }
}

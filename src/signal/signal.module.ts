import { Module, HttpModule } from '@nestjs/common';
import { SignalController } from './signal.controller';
import { SignalService } from './signal.service';
import { signal } from '../entities/signal';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SignalGateway } from './signal.gateway';
import { NotificationService } from '../notification/notification.service';
import { notification } from '../entities/notification';
import { CloudMessagingService } from '../common/utils/cloudMessaging.service';
import { trader } from '../entities/trader';

@Module({
  imports: [
    TypeOrmModule.forFeature([signal, notification, trader]),
    HttpModule
  ],
  controllers: [SignalController],
  providers: [
    SignalService,
    SignalGateway,
    NotificationService,
    CloudMessagingService
  ]
})
export class SignalModule { }

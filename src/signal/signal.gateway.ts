import { OnModuleInit } from '@nestjs/common';
import { WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { InjectEventEmitter } from 'nest-emitter';
import { SignalEventEmitter } from './signal.event';

@WebSocketGateway()
export class SignalGateway implements OnModuleInit {

    @WebSocketServer() server;

    constructor(
        @InjectEventEmitter() private readonly emitter: SignalEventEmitter
    ) { }

    onModuleInit() {
        this.emitter.on('signals', async msg => await this.onSignal(msg));
    }

    private async onSignal(signal) {
        this.server.emit(`signals`, signal);
    }
}
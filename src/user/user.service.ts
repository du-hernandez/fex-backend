import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getManager } from 'typeorm';

import { user } from '../entities/user';
import { UpdateUserDto } from './dto/updateUser.dto';
import { CreateUserDto } from './dto/createUser.dto';

@Injectable()
export class UserService {

    constructor(
        @InjectRepository(user) private readonly userRepository: Repository<user>
    ) { }

    async updateUser(id: number, user: UpdateUserDto) {
        try {
            let res = await this.userRepository.update(id, user);
            return res.raw.changedRows == 0 ? { error: 'NO_EXISTS' } : { success: 'OK' };
        } catch (error) {
            return { error: 'TRANSACTION_ERROR', detail: error };
        }
    }

    async createUser(user: CreateUserDto) {
        try {
            const res = await this.userRepository.save(user);
            return res.id == 0 ? { error: 'TRANSACTION_ERROR' } : { success: 'OK', detail: res.id };
        } catch (error) {
            return { error: 'TRANSACTION_ERROR', detail: error };
        }
    }
    
    async getUserAll() {
        return await this.userRepository.find()
    }
}

import { Controller, BadGatewayException, Post, Body, Put, Param, Get, UseGuards } from '@nestjs/common';

import { UserService } from './user.service';
import { UpdateUserDto } from './dto/updateUser.dto';
import { CreateUserDto } from './dto/createUser.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService) { }

    @Post()
    async createUser(@Body() body: CreateUserDto) {
        const response: any = await this.userService.createUser(body);
        if (response.success)
            return response;
        throw new BadGatewayException(response)
    }
    
    @Put(':id')
    async updateUser(@Param('id') id:number, @Body() body: UpdateUserDto) {
        const response: any = await this.userService.updateUser(id, body);
        if (response.success)
            return response;
        throw new BadGatewayException(response)
    }

    @Get()
    @UseGuards(AuthGuard('bearer'))
    async getTraderAll(){
        return this.userService.getUserAll();
    }
}

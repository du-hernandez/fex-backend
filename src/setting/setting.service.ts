import { Injectable } from '@nestjs/common';
import { setting } from '../entities/setting';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class SettingService {


    constructor(
        @InjectRepository(setting) private readonly settingRepository: Repository<setting>
    ) { }

    async updateNotificationState(userId: number) {
        try {
            const current = await this.settingRepository.findOne({ select: ["notifications", "id"], where: { user: userId } })

            const res = await this.settingRepository.update(
                current.id,
                { notifications: (current.notifications == 'active') ? 'inactive' : 'active' }
            );

            return res.raw.changedRows == 0 ? { error: 'NO_UPDATE' } : { success: 'OK' };
        } catch (error) {
            return { error: 'TRANSACTION_ERROR', detail: error }
        }
    }
}

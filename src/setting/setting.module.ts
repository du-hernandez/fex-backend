import { Module } from '@nestjs/common';
import { SettingController } from './setting.controller';
import { SettingService } from './setting.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { setting } from '../entities/setting';

@Module({
  imports: [
    TypeOrmModule.forFeature([setting])
  ],
  controllers: [SettingController],
  providers: [SettingService]
})
export class SettingModule {}

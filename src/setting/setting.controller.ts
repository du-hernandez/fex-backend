import { Controller, Put, UseGuards, Req, BadGatewayException } from '@nestjs/common';
import { SettingService } from './setting.service';
import { AuthGuard } from '@nestjs/passport';

@Controller('setting')
export class SettingController {
    constructor(private readonly settingService: SettingService) { }

    @Put('notifications-state')
    @UseGuards(AuthGuard('bearer'))
    async updateNotificationState(@Req() req) {
        const response: any = await this.settingService.updateNotificationState(req.user.id);
        if (response.success)
            return response;
        throw new BadGatewayException(response)
    }
}
